package com.usdglobal.javacode.pojo;

import java.util.ArrayList;
import java.util.Date;

public class Serverresponsepojo{
	   private int id;
	   private String firstName;
	   private String lastName;
	   private ArrayList<String> messages = new ArrayList<String>();
	   private String Statement;
	   int uppercase;
	   int lowercase;
	   int vowels;
	   int spaces;
	   public Serverresponsepojo()
	   {
		   
	   }
	   
	   public int getId()
	   {
	      return id;
	   }
	   
	   public void setId(int id)
	   {
	      this.id = id;
	   }
	   
	   public String getFirstName()
	   {
	      return firstName;
	   }
	   
	   public void setFirstName(String firstName)
	   {
	      this.firstName = firstName;
	   }
	   
	   public String getLastName()
	   {
	      return lastName;
	   }
	   
	   public void setLastName(String lastName)
	   {
	      this.lastName = lastName;
	   }
	   
	   public void setmessages(ArrayList<String> message)
	   {
		 this.messages=message;
	   }
	   
	   public ArrayList<String> getmessages()
	   {
		   return messages;
	   }
	   
	   public void setstatement(String Statement)
	   {
		   this.Statement=Statement;
	   }
	   
	   public String getstatement()
	   {
		   return Statement;
	   }
	   
	   public void setuppercase(int uppercase)
	   {
		   this.uppercase=uppercase;
	   }
	   
	   public void setlowercase(int lowercase)
	   {
		   this.lowercase=lowercase;
	   }
	   
	   public void setvowels(int vowels)
	   {
		   this.vowels=vowels;
	   }
	   
	   public void setspaces(int spaces)
	   {
		   this.spaces=spaces;
	   }	 
	   
	   public int getuppercase()
	   {
		   return uppercase;
	   }
	   
	   public int getlowercase()
	   {
		   return lowercase;
	   }
	   
	   public int getvowels()
	   {
		   return vowels;
	   }
	   
	   public int getspaces()
	   {
		   return spaces;
	   }
	   
	   
	   public Serverresponsepojo(int id, String firstName, String lastName,ArrayList<String> message,String statement,int uppercase,int lowercase,int vowels,int spaces)
	   {
		   this.id = id;
		   this.firstName = firstName;
		   this.lastName = lastName;
		   this.messages=message;
		   this.uppercase=uppercase;
		   this.lowercase=lowercase;
		   this.vowels=vowels;
		   this.spaces=spaces;
		   this.Statement=statement;
		}
	   public String getfirsname()
	   {
		   return firstName;
	   }
	   @Override
	   public String toString()
	   {
		 return "{Id: "+id + ", firstName:" + firstName + ", lastname:" + lastName +", Statement:" + Statement +", Uppercase:" + uppercase +", Lowercase:" + lowercase +", Vowels:" + vowels +", White Spaces:" + spaces + ", messages: "+messages.toString()+"}";
	   }
	   
}
