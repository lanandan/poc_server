package com.usdglobal.javacode.pojo;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
 
public class Serverrequestpojo
{
   private int id;
   private String firstName;
   private String lastName;
   private ArrayList<String> messages = new ArrayList<String>();
   private String Statement;
   
   private InetAddress ip;
   private String date;
   private String time;
   private String ipaddress;
   
   public Serverrequestpojo(){
 
   }
   
   public int getId()
   {
      return id;
   }
   public void setId(int id)
   {
      this.id = id;
   }
   public String getFirstName()
   {
      return firstName;
   }
   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }
   public String getLastName()
   {
      return lastName;
   }
   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }
 
   public void setmessages(ArrayList<String> message)
   { 
	   this.messages=message;
	
   
   }
   public ArrayList<String> getmessages()
   {
	   return messages;
   }
   
   public void setstatement(String statement)
   {
	   this.Statement=statement;
   }
   public String getstatement()
   {
	   return Statement;
   }
  
   public void setipaddress(String ipaddress)
   {
	   this.ipaddress=ipaddress;
	   
   }  
   
   public String getipaddress()
   {
    	 return ipaddress;
   }
     
   public void setdate(String date)
   {
	  this.date=date;
   }
   
   public String getdate()
   {
	   return date;
   }
   
   public void settime(String time)
   {
	   this.time=time;
   }
   
   public String settime()
   {
	   return time;
   }
  
   @Override
   public String toString()
   {
	
	  //return "Employees [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName +", Statement=" + Statement +", IPAddress=" + ipaddress +", date=" + date + ", time=" + time + ", messages= "+messages.toString()+" ]";
      return "Employees [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName +", Statement=" + Statement + ", messages= ["+messages.toString()+"] ]";
  
   
   }
}