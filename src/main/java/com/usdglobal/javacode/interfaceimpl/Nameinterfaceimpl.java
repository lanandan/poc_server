package com.usdglobal.javacode.interfaceimpl;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import com.usdglobal.javacode.abstractbase.Abstractbasicservice;
import com.usdglobal.javacode.interfaces.Serviceinterface;
import com.usdglobal.javacode.pojo.Serverrequestpojo;
import com.usdglobal.javacode.pojo.Serverresponsepojo;

@Path("/serverservice")
public class Nameinterfaceimpl extends Abstractbasicservice implements Serviceinterface{
	
	public static Serverresponsepojo stringservice=new Serverresponsepojo();;
	
	@Override
	@POST
    @Path("/stringcalculation")
    @Consumes("application/json")
	public Response Stringcalculation(Serverrequestpojo employee) {
       	String Statement=employee.getstatement();
    	int spacecount=0;
        int capitallettercount=0;
        int smallettercount=0;
        int vowelscount=0;
        int length=Statement.length();
        char content_count[]=new char[length];
        content_count=Statement.toCharArray();
         
        for(int i=0;i<length;i++){
            if(content_count[i]==' '){
             spacecount++;
         }
         
         int alphabets=(int)content_count[i];
        
         if(alphabets>=65&&alphabets<=90){
             capitallettercount++;
         }
         
         if(alphabets>=97&&alphabets<=122){
             smallettercount++;
         }
         
         if(alphabets==65||alphabets==97||alphabets==69||alphabets==101||alphabets==73||alphabets==105||alphabets==79||alphabets==111||alphabets==85||alphabets==117){
             vowelscount++;
         }
         
         }
        
        stringservice.setId(employee.getId()); 
        stringservice.setFirstName(employee.getFirstName());
        stringservice.setLastName(employee.getLastName());
        stringservice.setmessages(employee.getmessages());
        stringservice.setstatement(employee.getstatement());
        stringservice.setuppercase(capitallettercount);
        stringservice.setlowercase(smallettercount);
        stringservice.setspaces(spacecount);
        stringservice.setvowels(vowelscount);
        
        //stringservice=new Serverresponsepojo(employee.getId(),employee.getFirstName(),employee.getLastName(),employee.getmessages(),employee.getstatement(),capitallettercount,smallettercount,vowelscount,spacecount);
      
       
        
       return Response.status(200).entity("Success").build(); 
        
        
       
	}
	
	
	
	@GET
	@Path("/get")
	@Produces("application/json")
	public Serverresponsepojo getProductInJSON() {
 		
 
		return stringservice; 
 
	}
		
}
