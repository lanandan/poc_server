package com.usdglobal.javacode.abstractbase;

import java.net.InetAddress;
import java.util.ArrayList;

public abstract class Abstractbasicservice {

	   private String name;
	   private int id;
	   private String firstName;
	   private String lastName;
	   private ArrayList<String> messages = new ArrayList<String>();
	   private String Statement;
	   private InetAddress ip;
	   private String date;
	   private String time;
	   private String ipaddress; 
	   
	   public void setName(String name) {
		this.name = name;
	   }
 
	   public int getId()
	   {
	      return id;
	   }
	   public void setId(int id)
	   {
	      this.id = id;
	   }
	   public String getFirstName()
	   {
	      return firstName;
	   }
	   public void setFirstName(String firstName)
	   {
	      this.firstName = firstName;
	   }
	   public String getLastName()
	   {
	      return lastName;
	   }
	   public void setLastName(String lastName)
	   {
	      this.lastName = lastName;
	   }
	 
	   public void setmessages(ArrayList<String> message)
	   { 
		   this.messages=message;
	   }
	   public ArrayList<String> getmessages()
	   {
		   return messages;
	   }
	   
	   public void setstatement(String statement)
	   {
		   this.Statement=statement;
	   }
	   public String getstatement()
	   {
		   return Statement;
	   }
	  
	   public void setipaddress(String ipaddress)
	   {
		   this.ipaddress=ipaddress;
		   
	   }  
      
	   public String getipaddress()
       {
    	 return ipaddress;
       }
	    
	   public void setdate(String date)
	   {
		  this.date=date;
	   }
	   
	   public String getdate()
	   {
		   return date;
	   }
	   
	   public void settime(String time)
	   {
		   this.time=time;
	   }
	   
	   public String settime()
	   {
		   return time;
	   }

}
